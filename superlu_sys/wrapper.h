#ifndef WRAPPER_H
#define WRAPPER_H

#include "include/colamd.h"
#include "include/html_mainpage.h"
#include "include/slu_Cnames.h"
/* #include "include/slu_scomplex.h" */
/* #include "include/slu_dcomplex.h" */
/* #include "include/slu_sdefs.h" */
#include "include/slu_ddefs.h"
/* #include "include/slu_cdefs.h" */
/* #include "include/slu_zdefs.h" */
#include "include/slu_util.h"
/* #include "include/superlu_enum_consts.h" */
#include "include/supermatrix.h"

#endif
