extern crate pkg_config;
extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    println!(r"cargo:rustc-link-search=/usr/local/lib64");
    println!(r"cargo:rustc-link-search=/usr/lib64/llvm");
    println!(r"cargo:rustc-link-search=/usr/local/lib");
    println!(r"cargo:rustc-link-search=/usr/lib/llvm-3.8/lib");
    println!(r"cargo:rustc-link-lib=dylib=clang");
    println!(r"cargo:rustc-link-lib=static=superlu_5.1");
    pkg_config::Config::new().statik(true).probe("superlu_5.1").unwrap();

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .generate_comments(false)
        .header("wrapper.h")
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
