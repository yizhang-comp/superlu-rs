//! Rust binding for SuperLU.

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

extern crate libc;

// include bindings.rs from bindgen
#[link(name = "superlu_5.1", kind = "static")]
#[link(name = "clang", kind = "dylib")]
#[link(name = "gfortran")]
#[link(name = "scalapack", kind = "static")]
#[link(name = "lapack", kind = "dylib")]
#[link(name = "blas", kind = "dylib")]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

// RAII for SuperMatrix
impl Default for SuperMatrix {
    fn default() -> SuperMatrix {
        SuperMatrix {
            Stype: Stype_t::SLU_NC,
            Dtype: Dtype_t::SLU_D,
            Mtype: Mtype_t::SLU_GE,
            nrow: 0,
            ncol: 0,
            Store: ::std::ptr::null_mut(),
        }
    }
}

impl Default for superlu_options_t {
    fn default() -> superlu_options_t {
        superlu_options_t {
            Fact: fact_t::DOFACT,
            Equil: yes_no_t::NO,
            ColPerm: colperm_t::NATURAL,
            Trans: trans_t::NOTRANS,
            IterRefine: IterRefine_t::NOREFINE,
            DiagPivotThresh: 0.0,
            SymmetricMode: yes_no_t::NO,
            PivotGrowth: yes_no_t::NO,
            ConditionNumber: yes_no_t::NO,
            RowPerm: rowperm_t::NOROWPERM,
            ILU_DropRule: 0,
            ILU_DropTol: 0.0,
            ILU_FillFactor: 0.0,
            ILU_Norm: norm_t::TWO_NORM,
            ILU_FillTol: 0.0,
            ILU_MILU: milu_t::SILU,
            ILU_MILU_Dim: 0.0,
            ParSymbFact: yes_no_t::NO,
            ReplaceTinyPivot: yes_no_t::NO,
            SolveInitialized: yes_no_t::NO,
            RefineInitialized: yes_no_t::NO,
            PrintStat: yes_no_t::NO,
            nnzL: 0,
            nnzU: 0,
            num_lookaheads: 0,
            lookahead_etree: yes_no_t::NO,
            SymPattern: yes_no_t::NO,
        }
    }
}

impl Default for SuperLUStat_t {
    fn default() -> SuperLUStat_t {
        SuperLUStat_t {
            panel_histo: &mut vec![0][0] as *mut ::std::os::raw::c_int,
            utime: &mut vec![0.0][0] as *mut f64,
            ops: &mut 0.0,
            TinyPivots: 0,
            RefineSteps: 0,
            expansions: 0,
        }
    }
}

#[allow(unused_mut)]
#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    pub fn superlu() {
        let (mut A, mut L, mut U, mut B): (SuperMatrix,
                                           SuperMatrix,
                                           SuperMatrix,
                                           SuperMatrix) = (
            Default::default(),
            Default::default(),
            Default::default(),
            Default::default(),
        );

        // A.Stype = Stype_t::SLU_NC;
        // A.Dtype = Dtype_t::SLU_D;
        // A.Mtype = Mtype_t::SLU_GE;
        
        L.Stype = Stype_t::SLU_SC;
        L.Dtype = Dtype_t::SLU_D;
        L.Mtype = Mtype_t::SLU_TRLU;

        U.Stype = Stype_t::SLU_NC;
        U.Dtype = Dtype_t::SLU_D;
        U.Mtype = Mtype_t::SLU_TRU;

        let (mut s, mut u, mut p, mut e, mut r, mut l) =
            (19.0f64, 21.0f64, 16.0f64, 5.0f64, 18.0f64, 12.0f64);
        let mut m: ::std::os::raw::c_int = 5;
        let mut n: ::std::os::raw::c_int = 5;
        let mut nnz: ::std::os::raw::c_int = 12;

        // matrix A
        let mut a: Vec<f64> = vec![s, l, l, u, l, l, u, p, u, e, u, r];
        let mut asub: Vec<::std::os::raw::c_int> = vec![0, 1, 4, 1, 2, 4, 0, 2, 0, 3, 3, 4];
        let mut xa: Vec<::std::os::raw::c_int> = vec![0, 3, 6, 8, 10, 12];
        unsafe {
            dCreate_CompCol_Matrix(
                &mut A,
                m,
                n,
                nnz,
                &mut a[0] as *mut f64,
                &mut asub[0] as *mut ::std::os::raw::c_int,
                &mut xa[0] as *mut ::std::os::raw::c_int,
                Stype_t::SLU_NC,
                Dtype_t::SLU_D,
                Mtype_t::SLU_GE,
            );
        }

        // RHS
        let nrhs: ::std::os::raw::c_int = 1;
        let mut rhs: Vec<f64> = vec![1.0; (m * nrhs) as usize];
        unsafe {
            dCreate_Dense_Matrix(
                &mut B,
                m,
                nrhs,
                &mut rhs[0] as *mut f64,
                m,
                Stype_t::SLU_DN,
                Dtype_t::SLU_D,
                Mtype_t::SLU_GE,
            );
        }

        let mut options: superlu_options_t = Default::default();
        unsafe {
            set_default_options(&mut options);
        }
        options.ColPerm = colperm_t::NATURAL;

        /* Initialize the statistics variables. */
        let mut stat: SuperLUStat_t = Default::default();
        unsafe {
            StatInit(&mut stat);
        }

        // solve
        let mut perm_r: Vec<::std::os::raw::c_int> = vec![0; m as usize];
        let mut perm_c: Vec<::std::os::raw::c_int> = vec![0; n as usize];
        let mut info = 0;
        unsafe {
            dgssv(
                &mut options,
                &mut A,
                &mut perm_c[0] as *mut ::std::os::raw::c_int,
                &mut perm_r[0] as *mut ::std::os::raw::c_int,
                &mut L,
                &mut U,
                &mut B,
                &mut stat,
                &mut info,
            );
        }
        
        let mut cs = ::std::ffi::CString::new("Solution").unwrap().into_raw();
        unsafe {
            dPrint_Dense_Matrix(cs, &mut B);
        }

        unsafe {
            // Destroy_CompCol_Matrix(&mut A);
            Destroy_SuperMatrix_Store(&mut B);
            Destroy_SuperNode_Matrix(&mut L);
            Destroy_CompCol_Matrix(&mut U);
            StatFree(&mut stat);            
        }

    }
}
