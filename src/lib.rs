//! Rust binding for SuperLU, serial version.

#![cfg_attr(feature = "lint", feature(plugin))]
#![cfg_attr(feature = "lint", plugin(clippy))]
#![cfg_attr(feature = "lint", allow(block_in_if_condition_stmt, needless_return))]
#![cfg_attr(not(test), allow(dead_code))]

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused_mut)]

extern crate libc;
extern crate superlu_sys as lu;

// #[macro_use]
// extern crate lazy_static;

#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn lu_sys() {
        // let m: lu::SuperMatrix = Default::default();
    }
}
    
