# SuperLU-rs

Rust bindingsfor the SUPERLU library API.

Requires SUPERLU library of version 5.1 or later.

## Compatibility

### Platforms

`SuperLU-rs` is known to run on these platforms:

- Linux: x86_64 GNU/Linux

### Rust

`SuperLU-rs` is tested for all three official release channels: stable, beta and nightly.

## Building

### pkg-config script
`libsuperlu-sys` relies on `pkg-config` to find information
of `SuperLU` installation.

### SUPERLU version

Build scripts for both `libSuperLU-sys` and `SuperLU-rs` crates check the actual version of the
SUPERLU library that they are being linked against, and some functionality may be conditionally
enabled or disabled at compile time. While this allows supporting multiple versions of SUPERLU
in a single codebase, this is something the library user should be aware of in case they
choose to use the low level FFI bindings.

### Linux

The build script of `SuperLU_sys` crate will try to use `pkg-config` if it's available
to deduce SUPERLU library location. User must make sure
pkg-config is able to locate SuperLU static library.

Note that `cargo clean` is requred before rebuilding if any of those variables are changed.

## License

`SuperLU-rs` is primarily distributed under the terms of the
Apache License (Version 2.0). See [LICENSE-APACHE](LICENSE-APACHE)
for details.
